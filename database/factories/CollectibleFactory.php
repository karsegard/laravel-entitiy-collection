<?php

namespace KDA\Laravel\Entity\Collection\Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;
use KDA\Laravel\Entity\Collection\Models\EntityCollection;
use KDA\Laravel\Entity\Collection\Models\Relations\Collectible;

class CollectibleFactory extends Factory
{
    protected $model = Collectible::class;

    public function definition()
    {
        return [
            'entity_collection_id' => EntityCollection::factory(),
        ];
    }
}
