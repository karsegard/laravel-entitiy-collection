<?php

namespace KDA\Laravel\Entity\Collection\Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;
use KDA\Laravel\Entity\Collection\Models\EntityCollection;

class EntityCollectionFactory extends Factory
{
    protected $model = EntityCollection::class;

    public function definition()
    {
        return [
            'name' => $this->faker->word(),
            //   'sort'=>$this->faker->numberBetween(0,100)
        ];
    }
}
