<?php

namespace KDA\Laravel\Entity\Collection\Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;
use KDA\Laravel\Entity\Collection\Models\Entity;
use KDA\Laravel\Entity\Collection\Models\EntityType;

class EntityFactory extends Factory
{
    protected $model = Entity::class;

    public function definition()
    {
        return [
            'type_id' => EntityType::factory(),
            'name' => $this->faker->word(),

        ];
    }
}
