<?php

namespace KDA\Laravel\Entity\Collection\Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;
use KDA\Laravel\Entity\Collection\Models\EntityModel;

class EntityModelFactory extends Factory
{
    protected $model = EntityModel::class;

    public function definition()
    {
        return [
            'class' => $this->faker->word(),
            'name' => $this->faker->word(),
        ];
    }
}
