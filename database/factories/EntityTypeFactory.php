<?php

namespace KDA\Laravel\Entity\Collection\Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;
use KDA\Laravel\Entity\Collection\Models\EntityType;

class EntityTypeFactory extends Factory
{
    protected $model = EntityType::class;

    public function definition()
    {
        return [
            'name' => $this->faker->word(),
            'description' => $this->faker->paragraph(3, true),
        ];
    }
}
