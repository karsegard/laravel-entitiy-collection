<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::disableForeignKeyConstraints();

        Schema::create('entity_models', function (Blueprint $table) {
            $table->string('class')->primary();
            $table->string('name')->nullable();
            $table->timestamps();
        });

        Schema::create('entity_types', function (Blueprint $table) {
            $table->id();
            $table->string('name');
            $table->text('description')->nullable();
            $table->timestamps();
        });

        Schema::create('entities', function (Blueprint $table) {
            $table->id();
            $table->string('name')->nullable();
            $table->numericMorphs('model');
            $table->bigInteger('type_id');
            $table->timestamps();
            $table->unique(['model_id', 'model_type', 'type_id']);
        });

        Schema::create('entity_collections', function (Blueprint $table) {
            $table->id();
            $table->string('name');
            $table->bigInteger('type_id')->nullable();
            $table->boolean('locked')->default(true);
            $table->text('description')->nullable();
            $table->timestamps();
        });

        Schema::create('entity_collectibles', function (Blueprint $table) {
            $table->id();
            $table->bigInteger('entity_collection_id');
            $table->numericMorphs('entity');
            $table->integer('sort')->nullable();
            $table->timestamps();
        });

        Schema::enableForeignKeyConstraints();
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::disableForeignKeyConstraints();
        Schema::dropIfExists('entity_collections');

        Schema::enableForeignKeyConstraints();
    }
};
