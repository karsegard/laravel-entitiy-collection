<?php

namespace KDA\Laravel\Entity\Collection\Commands;

use Illuminate\Console\Command;
use Illuminate\Filesystem\Filesystem;
use KDA\Laravel\Entity\Collection\Facades\Entity;
use KDA\Laravel\Entity\Collection\Models\Entity as ModelsEntity;

class Generate extends Command
{
    /**
     * The console command name.
     *
     * @var string
     */
    protected $signature = 'kda:entities:generate {model}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Regenerate Entities';

    public function __construct(Filesystem $files)
    {
        parent::__construct();
    }

    public function fire()
    {
        return $this->handle();
    }

    /**
     * Execute the console command.
     *
     * @return void
     */
    public function handle()
    {
        $model = get_class(resolve($this->argument('model')));
        $records = $model::all();
        // dump($records);
        ModelsEntity::where('model_type', $model)->delete();
        $bar = $this->output->createProgressBar($records->count());
        foreach ($records as $model) {
            Entity::createForModel($model);
            $bar->advance();
        }
    }
}
