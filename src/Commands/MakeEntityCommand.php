<?php

namespace KDA\Laravel\Entity\Collection\Commands;


use Filament\Support\Commands\Concerns\CanManipulateFiles;
use Filament\Support\Commands\Concerns\CanValidateInput;
use Illuminate\Console\Command;
use Illuminate\Support\Str;

class MakeEntityCommand extends Command
{
    use CanManipulateFiles;
    use CanValidateInput;

    protected $signature = 'make:entity {name?} {--F|force}';

    protected $description = 'Create a new Entity';

    public function handle(): int
    {
        $pageBlock = (string) Str::of($this->argument('name') ?? $this->askRequired('Name (e.g. `AuthorEntity`)', 'name'))
            ->trim('/')
            ->trim('\\')
            ->trim(' ')
            ->replace('/', '\\');

        $pageBlockClass = (string) Str::of($pageBlock)->afterLast('\\');

        $pageBlockNamespace = Str::of($pageBlock)->contains('\\') ?
            (string) Str::of($pageBlock)->beforeLast('\\') :
            '';

        $shortName = Str::of($pageBlock)
            ->beforeLast('Block')
            ->explode('\\')
            ->map(fn ($segment) => Str::kebab($segment))
            ->implode('.');

       /* $view = Str::of($pageBlock)
            ->beforeLast('Block')
            ->prepend('components\\filament-blocks\\')
            ->explode('\\')
            ->map(fn ($segment) => Str::kebab($segment))
            ->implode('.');
        */
     //  $this->info ('view path :'.$view);
    
        $path = app_path(
            (string) Str::of($pageBlock)
                ->prepend('Entities\\')
                ->replace('\\', '/')
                ->append('.php'),
        );

        $this->info ($path);
      

        $files = [$path];
        if (! $this->option('force') && $this->checkForCollision($files)) {
            return static::INVALID;
        }

        $this->copyStubToApp('Entity', $path, [
            'class' => $pageBlockClass,
            'namespace' => 'App\\Entities' . ($pageBlockNamespace !== '' ? "\\{$pageBlockNamespace}" : ''),
            'shortName' => $shortName,
        ]);

      

        $this->info("Successfully created {$pageBlock}!");

        return static::SUCCESS;
    }
}