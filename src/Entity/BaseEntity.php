<?php


namespace KDA\Laravel\Entity\Collection\Entity;

abstract class BaseEntity
{
    abstract public static function type(): string;

    abstract static function models(): array;

    abstract static function getModelName($model): string;
    abstract static function getPrefix($model): ?string;

    public static function getName()
    {
        return static::type();
    }

    public static function getIndexedModel($model): string
    {
        $prefix = static::getPrefix($model);
        return !blank($prefix) ? "(" . $prefix . ") " . static::getModelName($model) : static::getModelName($model);
    }
}
