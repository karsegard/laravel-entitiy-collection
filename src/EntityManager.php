<?php

namespace KDA\Laravel\Entity\Collection;

use Illuminate\Support\Collection;
use KDA\Laravel\Entity\Collection\Models\Entity;
use KDA\Laravel\Entity\Collection\Models\EntityModel;
use KDA\Laravel\Entity\Collection\Models\EntityType;

use Illuminate\Filesystem\Filesystem;
use KDA\Laravel\Entity\Collection\Entity\BaseEntity;
use ReflectionClass;
use SplFileInfo;
use Str;
class EntityManager
{
    protected Collection $entities;
    protected Collection $registered_observers;
    

    public function __construct()
    {
        $this->entities = collect([]);
        $this->registered_observers = collect([]);
    }

    public function createForModel($model)
    {
        foreach ($model->getEntityTypes() as $type) {

            $name = $this->getModelName($model,$type);

            Entity::createWithType($type,$model,$name);
        }
    }

    public function updateModelIndex($model,$entity){
        $name = $this->getModelName($model,$entity);
        Entity::createWithType($entity::type(),$model,$name);
    }

    public function getModelName($model,$entity){
        $name = method_exists($model, 'getEntityName') ? $model->getEntityName($entity) : null;
        if(!$name){
            $name = $entity::getIndexedModel($model);
        }
        return $name;
    }

    public function deleteForModel($model)
    {
        Entity::where([
            'model_id' => $model->getKey(),
            'model_type' => get_class($model),
        ])->delete();
    }

    public function getModelsForTypes(array | string $types): Collection
    {
        $types = ! is_array($types) ? [$types] : $types;
        $results = collect([]);
        foreach ($types as $type) {
            $t = EntityType::where('name', $type)->first();

            $results = $results->merge($t->modelClasses);
        }

        return $results->unique();
    }

    public function getEntityModelName($type)
    {
        $t = EntityModel::where('class', $type)->first();

        return $t->name;
    }

    public function registerBlock(string $entity): void
    {
        if (!is_subclass_of($entity, BaseEntity::class)) {
            throw new \InvalidArgumentException("{$entity} must extend " . BaseBlock::class);
        }

        $this->entities->put($entity::getName(), $entity);
        collect($entity::models())->map(fn($model)=>$this->registerObservers($model,$entity));
    }

    public function register(string $class, string $baseClass): void
    {
        match ($baseClass) {
            BaseEntity::class => static::registerBlock($class),
            default => throw new \Exception('Invalid class type'),
        };
    }

    public function registerObservers($model,$entity){
        if(!$this->registered_observers->has($entity)){
            $this->registered_observers->put($entity,collect([]));
        }
        if( ! $this->registered_observers->get($entity)?->contains($model)){
            $model::created(function ($model)  use($entity) {
                $this->updateModelIndex($model,$entity);
            });
            $model::updating(function ($model) use($entity)  {
                $this->updateModelIndex($model,$entity);
            });
            $model::deleted(function ($model) use($entity) {
              //  $this->updateModelIndex($model,$entity);
                $this->deleteForModel($model);
            });
            $this->registered_observers->get($entity)->push($model);
        }
        
      //  $this->registered_observers = collect($observers);
    }

    public function registerFromDirectory(string $baseClass,  ?string $directory, ?string $namespace): void
    {
        if (blank($directory) || blank($namespace)) {
            return;
        }

        $filesystem = app(Filesystem::class);

        if ((!$filesystem->exists($directory)) && (!Str::of($directory)->contains('*'))) {
            return;
        }

        $namespace = Str::of($namespace);

            collect($filesystem->allFiles($directory))
                ->map(function (SplFileInfo $file) use ($namespace): string {
                    $variableNamespace = $namespace->contains('*') ? str_ireplace(
                        ['\\' . $namespace->before('*'), $namespace->after('*')],
                        ['', ''],
                        Str::of($file->getPath())
                            ->after(base_path())
                            ->replace(['/'], ['\\']),
                    ) : null;

                    return (string) $namespace
                        ->append('\\', $file->getRelativePathname())
                        ->replace('*', $variableNamespace)
                        ->replace(['/', '.php'], ['\\', '']);
                })
              /*  ->map(function($item){
                    dump($item);
                    return $item;
                })*/
                ->filter(function (string $class) use ($baseClass) : bool {
                   return  is_subclass_of($class, $baseClass) && (!(new ReflectionClass($class))->isAbstract());
                })
                ->each(fn (string $class) => $this->register($class, $baseClass));
              
    }
}
