<?php

namespace KDA\Laravel\Entity\Collection\Models\Contracts;

interface RepresentEntity
{
    public function getEntityTypes(): array;
}
