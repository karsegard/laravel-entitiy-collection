<?php

namespace KDA\Laravel\Entity\Collection\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use KDA\Laravel\Entity\Collection\Database\Factories\EntityFactory;

class Entity extends Model
{
    use HasFactory;

    protected $fillable = [
        'name',
        'model_type',
        'model_id',
        'type_id',
    ];

    protected $appends = [];

    protected $casts = [];

    public static function boot(): void
    {
        parent::boot();
        static::created(function ($model) {
            EntityModel::firstOrCreate(['class' => get_class($model->model)]);
        });
    }

    public static function createWithType($type, $model,$name)
    {
        $type = EntityType::firstOrCreate(['name' => $type]);
        return Entity::updateOrCreate(
            [
                'model_id' => $model->getKey(),
                'model_type' => get_class($model),
                'type_id' => $type->getKey(),
            ],
            ['name' => $name]
        );
    }

    protected static function newFactory()
    {
        return  EntityFactory::new();
    }

    public function scopeOfType($q, $type)
    {
        return $q->where('meta_type', resolve($type));
    }

    public function model()
    {
        return $this->morphTo();
    }

    public function type()
    {
        return $this->belongsTo(EntityType::class, 'type_id');
    }

    public function modelClass()
    {
        return $this->hasOne(EntityModel::class, 'class', 'model_type');
    }
}
