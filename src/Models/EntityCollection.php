<?php

namespace KDA\Laravel\Entity\Collection\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use KDA\Laravel\Entity\Collection\Database\Factories\EntityCollectionFactory;
use KDA\Laravel\Entity\Collection\Models\Relations\Collectible;
use KDA\Sluggable\Models\Contracts\RegisterSlugs;
use KDA\Sluggable\Models\Traits\HasSlugs;

class EntityCollection extends Model implements RegisterSlugs
{
    use HasFactory;
    use HasSlugs;

    protected $fillable = [
        'name',
    ];

    protected $appends = [

    ];

    protected $casts = [

    ];

    protected static function newFactory()
    {
        return  EntityCollectionFactory::new();
    }

    /**
     * HasSlugs methods
     */
    public function slugCollectionName()
    {
        return 'collections';
    }

    public function getSluggableAttribute()
    {
        return $this->name;
    }
    // End of HasSlug methods

    /** Relationships */
    public function entities()
    {
        return $this->hasMany(Collectible::class, 'entity_collection_id')->orderBy('sort');
    }

    public function getModelsAttribute()
    {
        return $this->load('entities.related')->entities->pluck('related');
    }

    public function type()
    {
        return $this->belongsTo(EntityType::class, 'type_id');
    }
}
