<?php

namespace KDA\Laravel\Entity\Collection\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use KDA\Eloquent\DefaultAttributes\Models\Traits\HasDefaultAttributes;
use KDA\Laravel\Entity\Collection\Database\Factories\EntityModelFactory;

class EntityModel extends Model
{
    use HasFactory;
    use HasDefaultAttributes;

    protected $primaryKey = 'class'; // or null

    public $incrementing = false;

    protected $fillable = [
        'name',
        'class',
    ];

    protected $appends = [

    ];

    protected $casts = [

    ];

    public function createDefaultAttributes()
    {
        if (! isset($this->attributes['name'])) {
            $this->attributes['name'] = (new \ReflectionClass($this->attributes['class']))->getShortName();
        }
    }

    protected static function newFactory()
    {
        return  EntityModelFactory::new();
    }

    public function entities()
    {
        return $this->hasMany(Entity::class, 'model_type');
    }
}
