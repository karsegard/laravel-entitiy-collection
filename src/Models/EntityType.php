<?php

namespace KDA\Laravel\Entity\Collection\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use KDA\Laravel\Entity\Collection\Database\Factories\EntityTypeFactory;

class EntityType extends Model
{
    use HasFactory;

    protected $fillable = [
        'name',
        'description',
    ];

    protected $appends = [

    ];

    protected $casts = [

    ];

    protected static function newFactory()
    {
        return  EntityTypeFactory::new();
    }

    public function entities()
    {
        return $this->hasMany(Entity::class, 'type_id');
    }

    public function modelClasses()
    {
        return $this->hasManyThrough(EntityModel::class, Entity::class, 'type_id', 'class', 'id', 'model_type');
    }
}
