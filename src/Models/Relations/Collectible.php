<?php

namespace KDA\Laravel\Entity\Collection\Models\Relations;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Relations\MorphPivot;
use KDA\Laravel\Entity\Collection\Database\Factories\CollectibleFactory;

class Collectible extends MorphPivot
{
    use HasFactory;

    protected $table = 'entity_collectibles';

    /* -------------------------------------------------------------------------- */
    /*                                  RELATIONS                                 */
    /* -------------------------------------------------------------------------- */

    public function collection()
    {
        return $this->belongsTo(EntityCollection::class);
    }

    public function related()
    {
        return $this->morphTo(__FUNCTION__, 'entity_type', 'entity_id');
    }

    protected static function newFactory()
    {
        return  CollectibleFactory::new();
    }
}
