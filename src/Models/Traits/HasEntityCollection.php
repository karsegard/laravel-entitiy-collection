<?php

namespace KDA\Laravel\Entity\Collection\Models\Traits;

use KDA\Laravel\Entity\Collection\Models\EntityCollection;
use KDA\Laravel\Entity\Collection\Models\Relations\Collectible;

trait HasEntityCollection
{
    public static function bootHasEntityCollection(): void
    {
        static::creating(function ($model) {
        });
        static::updating(function ($model) {
        });
    }

    /* public function entityCollections(){
        return $this->morphMany(EntityCollection::class,'entity_collectibles');
    }*/
    public function entityCollections()
    {
        return $this->morphToMany(EntityCollection::class, 'entity', 'entity_collectibles')
            ->using(Collectible::class);
    }
    /*
    public function has_many_polymorphic()
    {
        return $this->morphMany(Slug::class, 'sluggable');
    }

    public function has_one_of_many_polymorphic()
    {
        return $this->morphOne(Slug::class, 'sluggable')->latestOfMany();
    }

    public function scopeWithNoSlug($query)
    {
        return $query->whereDoesntHave('slugs');
     }*/
}
