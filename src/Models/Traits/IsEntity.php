<?php

namespace KDA\Laravel\Entity\Collection\Models\Traits;

use Illuminate\Support\Collection;
use KDA\Laravel\Entity\Collection\Facades\Entity as EntityFacade;
use  KDA\Laravel\Entity\Collection\Models\Entity;

trait IsEntity
{
    public static function bootIsEntity(): void
    {
        static::created(function ($model) {
            EntityFacade::createForModel($model);
        });
        static::updating(function ($model) {
        });
        static::deleted(function ($model) {
            EntityFacade::deleteForModel($model);
        });
    }

    public function entities()
    {
        return $this->morphMany(Entity::class, 'model');
    }

    public function getEntityTypesAttribute(): Collection
    {
        return $this->entities->pluck('type');
    }
}
