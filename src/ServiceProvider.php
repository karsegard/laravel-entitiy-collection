<?php

namespace KDA\Laravel\Entity\Collection;

use KDA\Laravel\Entity\Collection\Commands\Generate;
use KDA\Laravel\Entity\Collection\Commands\MakeEntityCommand;
use KDA\Laravel\Entity\Collection\Entity\BaseEntity;
use KDA\Laravel\Entity\Collection\Facades\Entity;
use KDA\Laravel\PackageServiceProvider;
use  KDA\Laravel\Traits\HasDumps;

//use Illuminate\Support\Facades\Blade;
class ServiceProvider extends PackageServiceProvider
{
    use \KDA\Laravel\Traits\HasCommands;
    use \KDA\Laravel\Traits\HasLoadableMigration;
    use HasDumps;

    protected $_commands = [
        Generate::class,
        MakeEntityCommand::class,
    ];

    protected $dumps = [
        'entity_models',
        'entity_types',
        'entities',
        'entity_collections',
        'entity_collectibles',
    ];

    /* This is mandatory to avoid problem with the package path */
    protected function packageBaseDir()
    {
        return dirname(__DIR__, 1);
    }

    //  trait \KDA\Laravel\Traits\HasLoadableMigration
    //  registers loadable and not published migrations
    // protected $migrationDir = 'database/migrations';
    public function register()
    {
        parent::register();

        $this->app->singleton(Entity::class, function () {
            return new EntityManager();
        });
    }

    /**
     * called after the trait were registered
     */
    public function postRegister()
    {
    }

    //called after the trait were booted
    protected function bootSelf()
    {
        Entity::registerFromDirectory(
            BaseEntity::class,
            app_path('Entities'),
            'App\Entities'
            
        );
    }
}
