<?php

namespace KDA\Tests\Entities;

class TileEntity
{
    public static function type(): string
    {
        return 'tile';
    }
}
