<?php

namespace KDA\Tests\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use  KDA\Laravel\Entity\Collection\Models\Traits\HasEntityCollection;

class Article extends Model
{
    use HasFactory;
    use HasEntityCollection;

    protected $fillable = [
        'title',
    ];

    protected static function newFactory()
    {
        return  \KDA\Tests\Database\Factories\ArticleFactory::new();
    }
}
