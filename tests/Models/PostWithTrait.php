<?php

namespace KDA\Tests\Models;

use KDA\Laravel\Entity\Collection\Models\Contracts\RepresentEntity;
use KDA\Laravel\Entity\Collection\Models\Traits\IsEntity;
use KDA\Tests\Entities\TileEntity;

class PostWithTrait extends Post implements RepresentEntity
{
    use IsEntity;

    protected $table = 'posts';

    protected static function newFactory()
    {
        return  \KDA\Tests\Database\Factories\PostWithTraitFactory::new();
    }

    public function getEntityTypes(): array
    {
        return [
            TileEntity::type(),
        ];
    }
}
