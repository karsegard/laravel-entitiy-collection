<?php

namespace KDA\Tests\Unit;

use Illuminate\Foundation\Testing\RefreshDatabase;
use KDA\Laravel\Entity\Collection\Models\Entity;
use KDA\Laravel\Entity\Collection\Models\EntityModel;
use KDA\Laravel\Entity\Collection\Models\EntityType;
use KDA\Laravel\Entity\Collection\Models\Relations\Collectible;
use KDA\Tests\Models\Post;
use KDA\Tests\Models\PostWithTrait;
use KDA\Tests\TestCase;

class ModelTest extends TestCase
{
    use RefreshDatabase;

    /** @test */
    public function can_create_collection()
    {
        $post = Post::factory()->create();

        $c = Collectible::factory()->create(['entity_id' => $post->id, 'entity_type' => get_class($post)]);

        $this->assertNotNull($c);
    }

    /** @test */
    public function can_create_entity_model()
    {
        $o = EntityModel::factory()->create();

        $this->assertNotNull($o);
    }

    /** @test */
    public function can_create_entity_type()
    {
        $o = EntityType::factory()->create();

        $this->assertNotNull($o);
    }

    /** @test */
    public function can_create_entity()
    {
        $post = Post::factory()->create();
        $o = Entity::factory()->create(['model_id' => $post->id, 'model_type' => get_class($post)]);

        $this->assertNotNull($o);
    }

    /** @test */
    public function entity_relationship()
    {
        $post = Post::factory()->create();
        EntityModel::factory()->create(['class' => get_class($post)]);

        $o = Entity::factory()->create(['model_id' => $post->id, 'model_type' => get_class($post)]);

        $this->assertNotNull($o);

        $this->assertNotNull($o->model);
        $this->assertNotNull($o->type);
        $this->assertNotNull($o->modelClass);
    }

    /** @test */
    public function entity_type_modelclasses_relationship()
    {
        // \DB::enableQueryLog(); // Enable query log

        $type = EntityType::factory()->create(['name' => 'tilable']);
        $post = Post::factory()->create();
        EntityModel::factory()->create(['class' => get_class($post)]);
        $entity = Entity::factory()->create(['model_id' => $post->id, 'model_type' => get_class($post), 'type_id' => $type->id]);

        $this->assertEquals(1, $type->fresh()->modelClasses->count());
    }

    /** @test */
    public function is_entity_trait()
    {
        \DB::enableQueryLog(); // Enable query log

        $post = PostWithTrait::factory()->create();

        $this->assertEquals(1, $post->entity_types->count());
        //  dd(\DB::getQueryLog());
    }
}
