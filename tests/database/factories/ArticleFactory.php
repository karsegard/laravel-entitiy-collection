<?php

namespace KDA\Tests\Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;
use KDA\Tests\Models\Article;

class ArticleFactory extends Factory
{
    protected $model = Article::class;

    public function definition()
    {
        return [
            'title' => $this->faker->sentence(4),
        ];
    }
}
