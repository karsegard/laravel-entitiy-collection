<?php

namespace KDA\Tests\Database\Factories;

use KDA\Tests\Models\PostWithTrait;

class PostWithTraitFactory extends PostFactory
{
    protected $model = PostWithTrait::class;
}
